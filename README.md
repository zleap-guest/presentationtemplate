# PresentationTemplate

This is a basic template for beamer presentations.
Files:

template.tex
openlogo-nd.pdf (added to each page)
Makefile

Contents

Title page
Contents page
Sample page
Thank you page to end presentation(s)

